define(
    ['knockout', 'jquery', 'pubsub', 'notifier', 'ccConstants', 'navigation', 'ccRestClient', 'storageApi', 'pageLayout/user'],
    function(ko, $, pubsub, notifier, CCConstants, navigation, ccRestClient, storageApi, UserViewModel){
        'use strict';
      
        var sessionStorageSupported = false;
        var LAST_LOGIN_TIMESTAMP_KEY = 'example_last_login';
        var LAST_LOGIN_TIMEOUT_THRESHOLD = 15;  /* in minutes */

        function parseParams() {
            var search = location.search.substring(1);
            
            //console.log('search');
            //console.log(search);
            return search ? JSON.parse('{"' + search.replace(/&/g, '","').replace(/=/g,'":"') + '"}',
                function(key, value) { return key === "" ? value:decodeURIComponent(value) }):{}
        }

        return {

            loginMessage: ko.observable(""),
            errorContainer: ko.observableArray([]),
            

            beforeAppear: function(page) {     
//
                console.log('Entering beforeAppear - login.js');   
                this.hideAllMessages(); 
                console.log('this.user().isUserSessionExpired - ' + this.user().isUserSessionExpired());
                if (this.user().isUserSessionExpired() && this.shouldShowTimeoutMessage()) {
                    this.user().handleLogout();
 	                this.errorContainer.push(this.translate("LOGOUT_INACTIVITY"));
 	                this.handleErrors();
                }       
                console.log('Exiting beforeAppear - login.js');             
            },
            
            onLoad: function(widget) {
                //console.log('login-load!');
                console.log('Entering onLoad - login.js');

                sessionStorageSupported = storageApi.getInstance().hasSessionStorageSupport();

                UserViewModel.prototype.getCurrentUser = function(isAutoLogin, isLogin) {
                    var self = this;
                    
                    // Reset in case it has been set by a previous operation.
                    self.redirectNotificationMessage('');

                    self.adapter.loadJSON('getUser', 'getCurrentUser', null,
                        //success callback
                        function(data) {
                            // Currently ignoring the 'links' property as it is not being used and causing problems in ccLink custom binding
                            ko.mapping.fromJS(data, {'ignore': ["links","dynamicProperties"]}, self);
                            self.populateUserViewModel(data);
                            self.resetModified();
                            self.readyToDisplay(true);
                            if (!isAutoLogin) {
                                if(isLogin && !self.loggedinAtCheckout()) {
                                    $.Topic(pubsub.topicNames.CART_READY).subscribe(function(){
                                        $.Topic(pubsub.topicNames.USER_LOAD_SHIPPING).publish([{message: "success"}]);
                                        $.Topic(pubsub.topicNames.CART_READY).unsubscribe();
                                    });
                                } else {
                                    $.Topic(pubsub.topicNames.USER_LOAD_SHIPPING).publish([{message: "success"}]);
                                }
                            } else {
                                $.Topic(pubsub.topicNames.AUTO_LOGIN_AND_GET_USER_DATA_SUCCESSFUL).publish();
                            }
                            if (self.sessionExpiredProfileRedirected) {
                                self.handleSessionExpired();
                                isLogin = true;
                                self.sessionExpiredProfileRedirected = false;
                            }
                            if(isLogin) {
                                if (sessionStorageSupported) {
                                    storageApi.getInstance().saveToSessionStorage(LAST_LOGIN_TIMESTAMP_KEY, Date.now());
                                }

                                if(data.parentOrganization && data.parentOrganization.name) {
                                    if(navigation.isPathEqualTo(self.checkoutHash)) {
                                        self.navigateToPage(self.checkoutHash);
                                    } else {
                                        $.Topic(pubsub.topicNames.CART_READY).subscribe(self.navigateToHome());
                                        /*$.Topic(pubsub.topicNames.CART_READY).subscribe(self.navigateToHome());*/
                                    }
                                }

                                $.Topic(pubsub.topicNames.USER_LOAD_CART).publish(data);
                            }

                        },
                        //error callback
                        function(data) {
                            console.log('1. isUserSessionExpired - ' + widget.user().isUserSessionExpired());
                            if (data.status == CCConstants.HTTP_UNAUTHORIZED_ERROR) {                 
                                if (widget.user().isUserSessionExpired() && this.shouldShowTimeoutMessage()) {
                                    widget.user().handleLogout();
                                    widget.hideAllMessages(); 
 	                                widget.errorContainer.push(widget.translate("LOGOUT_INACTIVITY"));
 	                                widget.handleErrors();
                                }  
                               
                                self.handleSessionExpired();                                                                     
                                if (navigation.isPathEqualTo(self.contextData.global.links.profile.route) || navigation.isPathEqualTo(self.contextData.global.links.cart.route) || navigation.isPathEqualTo(self.contextData.global.links.orderHistory.route)) {
                                    navigation.doLogin(navigation.getPath(), self.contextData.global.links.home.route);                          
                                }
                            }
                        }
                    );
                    
                    // Utility function to handle navigation to the home page when a B2B user logs in.
                    self.navigateToHome = function () {
                        var queryParams = parseParams();

                        setTimeout(function () {
                            if(queryParams.page) {
                                navigation.goTo(queryParams.page);
                            } else {
                                navigation.goTo('home');
                            }
                        }, 1000);

                        // Give the cart some time to load the user data before navigating to the new page.
                    };
                };
                
                
                widget.handleLoginFailure = function(e) {
                      console.log("login attempt failed");
                      widget.errorContainer.push(widget.translate("LOGIN_PASSWORD_INCORRECT"));
                      widget.handleErrors();
                };

                widget.shouldShowTimeoutMessage = function() {
                    if (!sessionStorageSupported) {
                        return false;
                        console.log('failing out of timeout message due to unsupported session storage');
                    }

                    try {
                        var currentTime = Date.now();
                        var lastLoginTime = new Date(parseInt(storageApi.getInstance().readFromSessionStorage(LAST_LOGIN_TIMESTAMP_KEY), 10));
                        lastLoginTime.setMinutes(lastLoginTime.getMinutes() + LAST_LOGIN_TIMEOUT_THRESHOLD);
                        var thresholdTime = lastLoginTime.getTime();

                        console.log('CT: ' + currentTime + '; TT: ' + thresholdTime + '; RESULT: ' + (currentTime <= thresholdTime));
                        return currentTime <= thresholdTime;
                    }
                    catch (e) {
                        return false;
                        console.log('failing out of timeout message due to exception');
                    }
                };
                
                $.Topic(pubsub.topicNames.USER_LOGIN_FAILURE).subscribe(widget.handleLoginFailure);
                $.Topic(pubsub.topicNames.USER_LOGOUT_SUCCESSFUL).subscribe(widget.handleLogout);
                   
                widget.clearErrors = function () {
                    widget.errorContainer([]);
                };
                
                // Generic function to clear error messages
                widget.hideAllMessages = function () {
                    $(".alert").hide();
                    widget.clearErrors();
                };
                
                widget.handleErrors = function () {
                    if(widget.errorContainer().length > 0){
                        var systemErrorString = "";
                        for (var i = 0; i < widget.errorContainer().length; i++) {
                            if (systemErrorString === "") {
                                systemErrorString = widget.errorContainer()[i];
                            } else if (!systemErrorString.includes(widget.errorContainer()[i])) {
                                systemErrorString += "<br/>" + widget.errorContainer()[i];
                            }
                        }
                        widget.loginMessage(systemErrorString);
                    }

                };
                console.log('Exiting onLoad - login.js');
            },

            isEnter: function(widget, data, event){
                if ("click" === event.type || (("keydown" === event.type || "keypress" === event.type) && event.keyCode === 13)) {
                    widget.handleLoginSubmit(widget);
                }
                return true;
            },

            handleLoginSubmit: function (widget) {
                widget.loginMessage("");
                widget.hideAllMessages();
                var login = widget.user().login();
                var pass = widget.user().password();
                if (login === '' || pass === '') {
                    if (login === '') {
                        widget.errorContainer.push(widget.translate("EMAIL_MISSING"));
                    }
                    if (pass === '') {
                        widget.errorContainer.push(widget.translate("PASSWORD_MISSING"));
                    }
                    widget.handleErrors();
                } else if (widget.user().validateLogin()) {
                    widget.user().updateLocalData(false, false);
                    //console.log('submit success!');
                    $.Topic(pubsub.topicNames.USER_LOGIN_SUBMIT).publishWith(widget.user(), [{message: "success"}]);
                } else {
                    //console.log("User invalid");
                    widget.handleLoginFailure();    
                }

                return true;
            },

            handleLogout: function() {
                console.log('login widget js handle logout');
                if (sessionStorageSupported) {
                    storageApi.getInstance().saveToSessionStorage(LAST_LOGIN_TIMESTAMP_KEY, 0);
                }
            }
        };
    }
);